class ErrorNotifier < ApplicationMailer
  require 'slim/smart'
  Slim::Engine.set_options smart_text_escaping: false

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.error_notifier.error.subject
  #
  def error(exception)
    @exception = exception
    @app_name = Rails.application.class.parent_name

    mail to: "sathors@free.fr", subject: "Error in #{@app_name}"
  end
end
