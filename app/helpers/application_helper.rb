module ApplicationHelper
  CONVERSION_DOLLAR_TO_EURO = 0.92

  def hidden_div_if(condition, attributes = {}, &block)
    if condition
      attributes["style"] = "display: none"
    end
    content_tag("div", attributes, &block)
  end

  def currency_to_locale(price)
    number_to_currency I18n.locale == :es ? \
      price * CONVERSION_DOLLAR_TO_EURO : price
  end

  def translated_options_for_select(values, translation_prefix)
    options_for_select(
      values.map do |key|
        I18n.translate("#{translation_prefix}.#{key.downcase}")
      end.zip(values)
    )
  end
end
