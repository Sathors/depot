class CopyProductPriceToLineItem < ActiveRecord::Migration
  def up
    add_column :line_items, :unit_price, :decimal

    LineItem.all.each do |line_item|
      line_item.unit_price = line_item.product.price
      line_item.save!
    end
  end

  def down
    remove_column :line_items, :unit_price, :decimal
  end
end
