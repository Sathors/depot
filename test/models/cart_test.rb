require 'test_helper'

class CartTest < ActiveSupport::TestCase
  setup do
    @cart = carts(:one)
  end

  test "total price should be working" do
    assert_equal 1, @cart.line_items.count
    assert_equal 9.99, @cart.total_price

    # Adding a line item.
    @cart.line_items << line_items(:two)
    assert_equal 2, @cart.line_items.count
    assert_equal 9.99*2, @cart.total_price

    # Changing the quantity of a line_item.
    @cart.line_items[0].quantity = 3
    assert_equal 9.99*4, @cart.total_price
  end

  test "add a product" do
    item = @cart.add_product(products(:one))
    assert_equal 2, item.quantity
  end
end
