require 'test_helper'

class UserStoriesTest < ActionDispatch::IntegrationTest
  fixtures :products, :orders

  test "buy a product" do
    login
    # A user goes to the index page.
    # They select a product, adding it to their cart, and check out, filling
    # in their information, along with a single line item corresponding to the
    # product they added to the cart.
    LineItem.delete_all
    Order.delete_all
    ruby_book = products(:ruby)

    # A user goes to the store index page.
    get '/'
    assert_response :success
    assert_template "index"

    # They select a product, adding it to their cart.
    xml_http_request :post, '/line_items', product_id: ruby_book.id
    assert_response :success

    cart = Cart.find(session[:cart_id])
    assert_equal 1, cart.line_items.size
    assert_equal ruby_book, cart.line_items[0].product

    # They then check out.
    get '/orders/new'
    assert_response :success
    assert_template 'new'

    post_via_redirect '/orders', order: {
      name: 'Dave Thomas',
      address: '123 The Street',
      email: 'dave@example.com',
      pay_type: 'Check',
    }
    assert_response :success
    assert_template 'index'
    cart = Cart.find(session[:cart_id])
    assert_equal 0, cart.line_items.size

    orders = Order.all
    assert_equal 1, orders.size
    order = orders[0]

    assert_equal 'Dave Thomas', order.name
    assert_equal '123 The Street', order.address
    assert_equal 'dave@example.com', order.email
    assert_equal 'Check', order.pay_type

    assert_equal 1, order.line_items.size
    line_item = order.line_items[0]
    assert_equal ruby_book, line_item.product

    mail = ActionMailer::Base.deliveries.last
    assert_equal ['dave@example.com'], mail.to
    assert_equal 'Sam Ruby <depot@example.com>', mail[:from].value
    assert_equal 'Pragmatic Store Order Confirmation', mail.subject
  end

  test "ship a product" do
    login
    put_via_redirect order_path(orders(:one)),
      order: { ship_date: '2015-01-01' }
    assert_response :success
    assert_template 'orders/show'

    mail = ActionMailer::Base.deliveries.last
    assert_equal ['dave@example.org'], mail.to
    assert_equal 'Sam Ruby <depot@example.com>', mail[:from].value
    assert_equal 'Pragmatic Store Order Shipped', mail.subject
  end

  test 'encounter an invalid cart' do
    login
    get '/carts/wibble'

    mail = ActionMailer::Base.deliveries.last
    assert_equal ['sathors@free.fr'], mail.to
    assert_equal 'from@example.com', mail[:from].value
    assert_equal 'Error in Depot', mail.subject
  end

  test 'cannot login without the password' do
    post login_path, name: 'dave', 'password': 'other_secret'
    get '/users'
    assert_redirected_to '/login?locale=en'
  end

  test 'can login in a userless system' do
    # Cannot access.
    get '/users'
    assert_redirected_to '/login?locale=en'

    # Deleting all users.
    User.all().delete_all

    # Can access with whatever credentials.
    post login_path, name: 'dave', 'password': 'secret'
    get '/users'
    assert_response :success
  end

  private
  def login
    post login_path, name: 'dave', 'password': 'secret'
  end
end
