class Order < ActiveRecord::Base
  has_many :line_items, dependent: :destroy

  def self.payment_types
    PaymentType.all.map(&:payment_type)
  end

  validates :name, :address, :email, presence: true
  validates :pay_type, inclusion: { in: payment_types }

  def add_line_items_from_cart(cart)
    cart.line_items.each do |item|
      item.cart_id = nil
      line_items << item
    end
  end
end
