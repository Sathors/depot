require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  setup do
    @user = users(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post :create, user: { name: 'Sam',
                            password: 'secret',
                            password_confirmation: 'secret' }
    end

    assert_redirected_to users_path
  end

  test "should show user" do
    get :show, id: @user
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @user
    assert_response :success
  end

  test "should update user" do
    assert @user.authenticate 'secret'
    patch :update, id: @user, current_password: 'secret',
      current_password_confirmation: 'secret',
      user: { name: 'bobby', password: 'new_secret',
              password_confirmation: 'new_secret' }
    assert_equal 'bobby', assigns(:user).name
    assert_redirected_to users_path
    assert assigns(:user).authenticate 'new_secret'
  end

  test "should not let update user" do
    # The current password confirmation does not match the current password.
    patch :update, id: @user,
      current_password: 'secret',
      current_password_confirmation: 'other_secret',
      user: { name: 'bobby' , password: 'new_secret',
              password_confirmation: 'new_secret' }
    assert_template :edit
    assert_equal ["Wrong current password confirmation"],
      assigns(:user).errors[:base]

    # The current password is not valid
    patch :update, id: @user, current_password: 'wrong_secret',
      current_password_confirmation: 'wrong_secret',
      user: { name: 'bobby', password: 'new_secret',
              password_confirmation: 'new_secret' }
    assert_template :edit
    assert_equal ["The current password is wrong"],
      assigns(:user).errors[:base]
  end

  test "should destroy user" do
    assert_difference('User.count', -1) do
      delete :destroy, id: @user
    end

    assert_redirected_to users_path
  end
end
