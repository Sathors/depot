class StoreController < ApplicationController
  include CurrentCart
  before_action :set_cart
  skip_before_action :authorize

  def index
    if params[:set_locale]
      redirect_to store_url(locale: params[:set_locale])
    else
      @products = Product.where(locale: [I18n.locale, nil]).order(:title)
      if session[:counter].nil?
        session[:counter] = 1
      else
        session[:counter] += 1
      end
    end
  end
end
